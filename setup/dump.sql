--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: SpaceRoyale; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "SpaceRoyale";


ALTER SCHEMA "SpaceRoyale" OWNER TO postgres;

--
-- Name: SCHEMA "SpaceRoyale"; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA "SpaceRoyale" IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: users; Type: TABLE; Schema: SpaceRoyale; Owner: postgres
--

CREATE TABLE "SpaceRoyale".users (
    userid integer NOT NULL,
    nickname character varying NOT NULL,
    email character varying NOT NULL,
    hash character varying NOT NULL,
    currency integer DEFAULT 0 NOT NULL,
    skinid integer DEFAULT 0 NOT NULL
);


ALTER TABLE "SpaceRoyale".users OWNER TO postgres;

--
-- Name: Users_key_seq; Type: SEQUENCE; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE "SpaceRoyale".users ALTER COLUMN userid ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME "SpaceRoyale"."Users_key_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: inventoryitems; Type: TABLE; Schema: SpaceRoyale; Owner: postgres
--

CREATE TABLE "SpaceRoyale".inventoryitems (
    itemid integer DEFAULT 0 NOT NULL,
    userid integer NOT NULL
);


ALTER TABLE "SpaceRoyale".inventoryitems OWNER TO postgres;

--
-- Name: player_stats; Type: TABLE; Schema: SpaceRoyale; Owner: postgres
--

CREATE TABLE "SpaceRoyale".player_stats (
    userid integer NOT NULL,
    total_score integer DEFAULT 0 NOT NULL,
    best_score integer DEFAULT 0 NOT NULL,
    total_kills integer DEFAULT 0 NOT NULL,
    best_kills integer DEFAULT 0 NOT NULL,
    total_deaths integer DEFAULT 0 NOT NULL,
    death_by_asteroid_crash integer DEFAULT 0 NOT NULL,
    death_by_collision_with_other_ship integer DEFAULT 0 NOT NULL,
    death_by_enemy_shot integer DEFAULT 0 NOT NULL,
    death_by_own_shot integer DEFAULT 0 NOT NULL
);


ALTER TABLE "SpaceRoyale".player_stats OWNER TO postgres;

--
-- Name: shopitems; Type: TABLE; Schema: SpaceRoyale; Owner: postgres
--

CREATE TABLE "SpaceRoyale".shopitems (
    itemid integer NOT NULL,
    imgname character varying NOT NULL,
    price integer DEFAULT 0 NOT NULL
);


ALTER TABLE "SpaceRoyale".shopitems OWNER TO postgres;

--
-- Data for Name: inventoryitems; Type: TABLE DATA; Schema: SpaceRoyale; Owner: postgres
--

COPY "SpaceRoyale".inventoryitems (itemid, userid) FROM stdin;
\.


--
-- Data for Name: player_stats; Type: TABLE DATA; Schema: SpaceRoyale; Owner: postgres
--

COPY "SpaceRoyale".player_stats (userid, total_score, best_score, total_kills, best_kills, total_deaths, death_by_asteroid_crash, death_by_collision_with_other_ship, death_by_enemy_shot, death_by_own_shot) FROM stdin;
\.


--
-- Data for Name: shopitems; Type: TABLE DATA; Schema: SpaceRoyale; Owner: postgres
--

COPY "SpaceRoyale".shopitems (itemid, imgname, price) FROM stdin;
4	saucerGreen	0
0	skin00	0
1	skin01_whiteRed	1
3	skin02	5
2	skin01_whiteBlue	1
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: SpaceRoyale; Owner: postgres
--

COPY "SpaceRoyale".users (userid, nickname, email, hash, currency, skinid) FROM stdin;
\.


--
-- Name: Users_key_seq; Type: SEQUENCE SET; Schema: SpaceRoyale; Owner: postgres
--

SELECT pg_catalog.setval('"SpaceRoyale"."Users_key_seq"', 68, true);


--
-- Name: shopitems ShopItems_pkey1; Type: CONSTRAINT; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE ONLY "SpaceRoyale".shopitems
    ADD CONSTRAINT "ShopItems_pkey1" PRIMARY KEY (itemid);


--
-- Name: users Users_pkey; Type: CONSTRAINT; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE ONLY "SpaceRoyale".users
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (userid);


--
-- Name: inventoryitems inventoryitems_pkey; Type: CONSTRAINT; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE ONLY "SpaceRoyale".inventoryitems
    ADD CONSTRAINT inventoryitems_pkey PRIMARY KEY (itemid, userid);


--
-- Name: player_stats player_stats_pkey; Type: CONSTRAINT; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE ONLY "SpaceRoyale".player_stats
    ADD CONSTRAINT player_stats_pkey PRIMARY KEY (userid);


--
-- Name: inventoryitems itemid; Type: FK CONSTRAINT; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE ONLY "SpaceRoyale".inventoryitems
    ADD CONSTRAINT itemid FOREIGN KEY (itemid) REFERENCES "SpaceRoyale".shopitems(itemid) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: users skinid; Type: FK CONSTRAINT; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE ONLY "SpaceRoyale".users
    ADD CONSTRAINT skinid FOREIGN KEY (skinid) REFERENCES "SpaceRoyale".shopitems(itemid) ON UPDATE SET DEFAULT ON DELETE SET DEFAULT NOT VALID;


--
-- Name: player_stats userid; Type: FK CONSTRAINT; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE ONLY "SpaceRoyale".player_stats
    ADD CONSTRAINT userid FOREIGN KEY (userid) REFERENCES "SpaceRoyale".users(userid) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: inventoryitems userid; Type: FK CONSTRAINT; Schema: SpaceRoyale; Owner: postgres
--

ALTER TABLE ONLY "SpaceRoyale".inventoryitems
    ADD CONSTRAINT userid FOREIGN KEY (userid) REFERENCES "SpaceRoyale".users(userid) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- PostgreSQL database dump complete
--

