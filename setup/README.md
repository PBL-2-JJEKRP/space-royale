# Deployment

Below are the steps needed to deploy SpaceRoyale server.

## Hosts file

Ansible hosts group variables for the deployment scripts:

```yaml
[Server]
10.0.x.xxx
```

The file can be found within an Ansible repository or in ```/etc/ansible/hosts```

## Server Deployment

```ansible-playbook Server.yml --tags "config"```

## Automatic Server Update

For CI/CD pipeline ```--tags "pull"``` can be used.

Script example

```ansible-playbook Server.yml --tags "pull"```