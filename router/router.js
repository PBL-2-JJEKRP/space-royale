const express = require("express");
const router = express.Router();
const passport = require("passport");
const { database } = require("../utils/dbConfig");
const { checkAuth, checkNotAuth } = require("../utils/middleware");
const { checkNickname, checkEmail, addUser } = require("../dataBase/user"); //check if user nickname or username is occupied
let { getUsers, getPersonalScores } = require("../dataBase/players");
let {
  getShopItems,
  getOwnedItems,
  getItemById,
  selectSkin,
  checkBalance,
  buyItem,
} = require("../dataBase/shop");

let user;

// on signup
router.all("/", (req, res) => {
  if (req.isAuthenticated()) {
    res.redirect("/user");
  } else {
    res.render("index");
  }
});

/**
 * ------------ GET ROUTES ------------
 */

router.get("/user", checkAuth, async (req, res) => {
  user = req.user;
  const skin = await getItemById(user.skinid);
  res.render("user", {
    user: req.user,
    players: await getUsers(),
    stats: await getPersonalScores(user.userid),
    userSkinName: skin.imgname,
  });
});

router.get("/user/shop", checkAuth, async (req, res) => {
  user = req.user;
  //await buyItem(user.userid, 1);
  res.render("shop", {
    user: req.user,
    skins: await getShopItems(user.userid),
  });
});

router.get("/user/owned-skins", checkAuth, async (req, res) => {
  user = req.user;

  res.render("owned", {
    user: req.user,
    skins: await getOwnedItems(user.userid),
  });
});

router.get(
  "/user/shop/buy-product/:id",
  checkAuth,
  checkBalance,
  buyItem,
  async (req, res) => {
    //console.log("success");
    //console.log(await buyItem(req.user.userid, req.params.id));
    res.redirect("/user/shop");
  }
);

router.get("/user/shop/select-skin/:id", checkAuth, async (req, res) => {
  await selectSkin(req.user.userid, req.params.id);
  res.redirect("/user");
});

router.get("/login", checkNotAuth, (req, res) => {
  res.render("login");
});

router.get("/game", (req, res) => {
  res.redirect("/");
});

router.get("/signup", checkNotAuth, (req, res) => {
  res.render("signup");
});

router.get("/logout", (req, res) => {
  req.logOut();
  res.redirect("/");
});

/**
 * ------------ POST ROUTES ------------
 */

router.post(
  "/login",
  checkNotAuth,
  passport.authenticate("local", {
    successRedirect: "/user",
    failureRedirect: "/login",
    failureFlash: true,
  })
);

router.post("/game", async (req, res) => {
  let nickname = req.body.nickname;
  let skinName = "public/assets/img/skins/skin00.png";
  let user = req.user;
  let id
  if (user && user.skinid != null) {
    let skin = await getItemById(req.user.skinid);
    skinName = "public/assets/img/skins/" + skin.imgname + ".png";
  }
  if(!user)
    id = null
  else if(user)
    id = user.userid

  if (nickname.length === 0 || nickname.trim().length === 0) {
    nickname = null;
    if (user) {
      nickname = user.nickname;
    }
  }

  res.render("game", {
    user: {
      id: id,
      nickname: nickname,
      ship: skinName,
    },
    layout: __dirname + "/../public/views/game",
  });
});

router.post(
  "/signup",
  checkNotAuth,
  checkNickname,
  checkEmail,
  addUser,
  async (req, res) => {
    return res.render("login", {
      success: "You have successfully created an account",
    });
  }
);



module.exports = {
  router: router,
};
