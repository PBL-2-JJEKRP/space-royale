(function () {
  "use strict";

  $(function () {
    $.validator.addMethod("validEmail", function (value, element) {
      return this.optional(element) || /^[@.\w]+$/i.test(value);
    });

    $("form[name='login']").validate({
      rules: {
        email: {
          required: true,
          email: true,
          maxlength: 255,
          validEmail: true,
        },
        password: {
          required: true,
          maxlength: 50,
        },
      },

      messages: {
        email: "Please enter a valid email address",

        password: {
          required: "Please enter password",
        },
      },

      submitHandler: function (form) {
        form.submit();
      },
    });
  });

  $(function () {
    $.validator.addMethod("char", function (value, element) {
      return this.optional(element) || /^[@.\w]+$/i.test(value);
    });

    $("form[name='registration']").validate({
      rules: {
        nickname: {
          required: true,
          maxlength: 14,
          char: true,
        },
        email: {
          required: true,
          email: true,
          maxlength: 255,
          char: true,
        },
        password: {
          required: true,
          minlength: 5,
          maxlength: 50,
        },
        conf_password: {
          required: true,
          equalTo: "#password",
          maxlength: 50,
        },
      },

      messages: {
        nickname: {
          required: "Please enter your nickname",
          maxLength: "Nickname is too long",
          char: "Only letters, numbers, and '.@_' are allowed",
        },
        email: "Please enter a valid email address",
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long",
          maxlength: "Your password is too long",
        },
        conf_password: {
          required: "Please repeat the password",
          equalTo: "Please make sure your passwords match",
          maxlength: "Your password is too long",
        },
      },

      submitHandler: function (form) {
        form.submit();
      },
    });
  });

  $(function () {
    $("form[name='nickname']").validate({
      rules: {
        nickname: {
          maxlength: 14,
        },
      },

      messages: {
        nickname: {
          maxLength: "Nickname is too long",
        },
      },

      submitHandler: function (form) {
        form.submit();
      },
    });
  });
})();
