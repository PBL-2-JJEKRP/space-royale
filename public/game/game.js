import createStarfield from "./starsBackground.js ";

const config = {
  type: Phaser.AUTO,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,


    //width: 6400,
    //height: 3200,
    width: 2400, // How far can the player see
    height: 1350,
    min: {
      width: 512, // Minimal size of game window. Trying to make it smaller will make the window scrollable
      height: 288,
    },
  },
  scene: {
    preload: preload,
    create: create,
    update: update,
  },
  physics: {
    default: "arcade",
    arcade: {
      debug: false,
      gravity: {
        x: 0,
        y: 0,
      },
    },
  },
};

const game = new Phaser.Game(config);

let keyA, keyD, keyW, keyX, keySpace;

const baseImgPath = "assets/img";
const shipSkinImgPath = `${baseImgPath}/skins`

function preload() {
  this.load.image('skin00', `${shipSkinImgPath}/skin00.png`)
  this.load.image('skin01_whiteRed', `${shipSkinImgPath}/skin01_whiteRed.png`)
  this.load.image('skin01_whiteBlue', `${shipSkinImgPath}/skin01_whiteBlue.png`)
  this.load.image('skin02', `${shipSkinImgPath}/skin02.png`)
  this.load.image("saucerGreen", `${shipSkinImgPath}/saucerGreen.png`);
  this.load.image("bullet", `${baseImgPath}/bullet.png`);
  this.load.image("asteroid", `${baseImgPath}/asteroid.png`);
  this.load.image("star", `${baseImgPath}/star_gold.png`);
  this.load.image("backgroundStarSmall", `${baseImgPath}/backgroundStar1.png`);
  this.load.image("backgroundStarBig", `${baseImgPath}/backgroundStar2.png`);
  this.load.spritesheet("explosion", `${baseImgPath}/spaceshipDied.png`, {frameWidth : 96, frameHeight : 96});
  this.load.image("gameOverWindow", `${baseImgPath}/gameOverWindow.png`);
  this.load.image("restartButton", `${baseImgPath}/restartButton.png`);
  this.load.image("menuButton", `${baseImgPath}/menuButton.png`);
}

function create() {
  const self = this;
  window.scene = this;
  keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
  keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  keyW = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
  keyX = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.X);
  keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

  this.socket = io();
  this.otherPlayers = this.physics.add.group();
  this.bullets = this.physics.add.group();
  this.asteroids = this.physics.add.group();
  this.scoreboardSize;
  this.scoreboard = [];
  this.user = JSON.parse(document.getElementById("gameScript").getAttribute("user"));
  this.socket.emit("sendUser", this.user);
  this.clientScore = 0
  this.clientScoreText = 'Ordinary Pilot';
  this.clientInTop = false;
  this.fontSize = Math.floor(this.renderer.width / 75); // Size of font is relative to window size
  this.leaderboardGapY = Math.floor(this.fontSize * 1.5);
  this.leaderboardWidth = this.fontSize * 17;
  this.leaderboardHeigth = (this.fontSize + this.leaderboardGapY) * 0.6; // Needs to be multiplied by scoreboard size
  this.leaderboardX = this.renderer.width - this.leaderboardWidth;
  this.leaderboardY = 16;
  this.enemyFontColor = "#EEE7E5"; // White
  this.clientFontColor = "#d74317"; // Orange
  this.gameOverX = Math.floor(this.renderer.width / 2);
  this.gameOverY = Math.floor(this.renderer.height / 2);

  let explosionConfig = {
    key: 'explosion',
    frames: this.anims.generateFrameNumbers('explosion', { start: 0, end: 11, first: 0 }),
    frameRate: 12,
    hideOnComplete: true,
    repeat: 0
  };

  this.anims.create(explosionConfig);

  //Draws players
  this.socket.on("currentPlayers", function (players, invulnerability) {
    Object.keys(players).forEach(function (id) {
      if (players[id].playerId === self.socket.id) {
        addPlayer(self, players[id]);
        setTimeout(function() {asteroidPlayerOverlap(self)}, invulnerability);
        bulletPlayerOverlap(self);
      } else {
        addOtherPlayers(self, players[id]);
      }
    });
  });
  // compares the positions of bullets in the client and in the server
  // redraws them in correct locations if significant differences are found
  this.socket.on("bulletUpdate", function (bulletData, maximumBulletDeviation) {
    let id;
    self.bullets.getChildren().forEach(function (bullet) {
      id = bullet.bulletId;
      // If bullet exists in server
      if (typeof bulletData[id] == `undefined`) {
        bullet.destroy();
        return;
      }
      if (  // If bullet is desynchronized - redraw it
        Phaser.Math.Distance.Between(
          bulletData[id].x, bulletData[id].y,
          bullet.x, bullet.y
        ) > maximumBulletDeviation) {
        bullet.x = bulletData[id].x;
        bullet.y = bulletData[id].y;
      }
    });
  });

  this.socket.on("asteroidUpdate", function (asteroidData, maximumAsteroidDeviation) {
    let id;
    self.asteroids.getChildren().forEach(function (asteroid) {
      id = asteroid.asteroidId;
      let serverAsteroid = asteroidData[id];
      // If asteroid exists in server
      if (typeof serverAsteroid == `undefined`){
        asteroid.destroy();
        return;
      }
      if (  // If asteroid is desynchronized - redraw it
        Phaser.Math.Distance.Between(
          serverAsteroid.x, serverAsteroid.y,
          asteroid.x, asteroid.y
        ) > maximumAsteroidDeviation
      ) {
        asteroid.x = serverAsteroid.x
        asteroid.y = serverAsteroid.y
      }
    });
    }
  );

  this.socket.on("newPlayer", function (playerInfo, playerId) {
    if (self.socket.id != playerId) {
      addOtherPlayers(self, playerInfo);
    }
  });

  this.socket.on("disconnect", function (playerId) {
    self.otherPlayers.getChildren().forEach(function (otherPlayer) {
      if (playerId === otherPlayer.playerId) {
        otherPlayer.destroy();
      }
    });
  });

  //Draws player movement
  this.socket.on("playerMoved", function (playerInfo) {
    self.otherPlayers.getChildren().forEach(function (otherPlayer) {
      if (playerInfo.playerId === otherPlayer.playerId) {
        otherPlayer.setRotation(playerInfo.rotation);
        otherPlayer.setPosition(playerInfo.x, playerInfo.y);
      }
    });
  });

  // Destroys a dead player's ship
  this.socket.on("playerDied", function (playerId) {
    // Finds the player who died
    self.otherPlayers.getChildren().forEach(function (otherPlayer) {
      if (playerId == otherPlayer.playerId) {
        let deathSprite = self.add.sprite(otherPlayer.x, otherPlayer.y, 'explosion')
        deathSprite.play('explosion');
        otherPlayer.destroy();
        return;
      }
    });
    //let kills = player[playerId].kills;
    let score = self.clientScore
    // If the player who died is the client
    if (playerId == self.socket.id) {
      let deathSprite = self.add.sprite(self.ship.x, self.ship.y, 'explosion')
      deathSprite.play('explosion');
      self.ship.destroy();
      self.time.addEvent({
        delay: 1000,
        callback: ()=>{
          showGameOverScene("You died! \n Score:" + score, + score, 1000, 500);
        },
      })
    }
  });

  function showGameOverScene(text, w = 1000, h = 500) {
    let bg = self.add.image(self.gameOverX, self.gameOverY, 'gameOverWindow').setScrollFactor(0, 0).setAlpha(0.5).setDepth(10).setOrigin(0.5, 0.5);
    bg.width = w;
    bg.height = h;
    self.add.text(self.gameOverX, self.gameOverY - 40, text, {fontSize: '30px', color: '#000'}).setScrollFactor(0, 0).setDepth(10).setOrigin(0.5, 0.5);
    let restartButton = self.add.image(self.gameOverX + 100, self.gameOverY + 70, 'restartButton').setScrollFactor(0, 0).setAlpha(0.9).setDepth(10).setOrigin(0.5, 0.5).setDisplaySize(192, 72);
    restartButton.setInteractive();
    restartButton.on('pointerdown', function () {
      self.gameOver = true;
    });
    let menuButton = self.add.image(self.gameOverX - 100, self.gameOverY + 70, 'menuButton').setScrollFactor(0, 0).setAlpha(0.9).setDepth(10).setOrigin(0.5, 0.5).setDisplaySize(192, 72);
    menuButton.setInteractive();
    menuButton.on('pointerdown', function() {
      self.returnToMenu = true;
    })
  };

  this.socket.on('initiateOnDeathDataTransferToDb', function(playerData) {
    self.socket.emit('I died', playerData);
  })

  // Destroys a dead bullet's body
  this.socket.on("bulletDied", function (bulletId) {
    // Finds the bullet which died
    self.bullets.getChildren().forEach(function (bullet) {
      if (bulletId == bullet.bulletId) {
        bullet.destroy();
        return;
      }
    });
  });

  // Destroys a dead asteroid's body
  this.socket.on("asteroidDied", function (asteroidId) {
    // Finds the asteroid that died
    self.asteroids.getChildren().forEach(function (asteroid) {
      if (asteroidId == asteroid.asteroidId) {
        asteroid.destroy();
        return;
      }
    });
  });

  // Draws all bullets upon connecting to a server
  this.socket.on("currentBullets", function (bullets) {
    Object.keys(bullets).forEach(function (id) {
      addBullet(self, bullets[id]);
    });
  });

  // Draws all asteroids.js upon connecting to a server
  this.socket.on("currentAsteroids", function (asteroidData) {
    Object.keys(asteroidData).forEach(function (id) {
      addAsteroid(self, asteroidData[id]);
    });
  });

  this.socket.on("asteroidAdded", function (asteroidData) {
    addAsteroid(self, asteroidData);
  });

  // Draws a bullet after it has been fired
  this.socket.on("bulletFired", function (bulletData) {
    addBullet(self, bulletData);
  });

  this.cursors = this.input.keyboard.createCursorKeys();

  this.socket.on('updateClientScore', function(nick, newScore){
    updateClientScore(self, nick, newScore);
  });
   
  this.socket.on("scoreUpdate", function (newScoreboard) {
    updateScoreboard(self, self.scoreboard, newScoreboard, self.scoreboardSize);
  });

  //Draws a star
  this.socket.on("starLocation", function (star) {
    if (self.star) self.star.destroy();
    self.star = self.physics.add.image(star.x, star.y, "star");
    self.star.id = star.id;
    self.physics.add.overlap(
      self.ship,
      self.star,
      function () {
        this.socket.emit("starCollected", self.star.id);
      },
      null,
      self
    );
  });
  //Draws background stars, sets map's and camera's bounds
  this.socket.on("sendServerConfiguration", function (mapWidth, mapHeight, scoreboardSize) {
    createStarfield(mapWidth, mapHeight);
    window.scene.physics.world.setBounds(0, 0, mapWidth, mapHeight);
    scene.cameras.main.setBounds(0, 0, mapWidth, mapHeight);
    self.scoreboardSize = scoreboardSize;
    drawEmptyScoreboard(self, self.scoreboardSize);
    addClientScore(self);
  });
}

function drawEmptyScoreboard(self, size){ //draws an empty scoreboard
  self.add.rectangle(self.leaderboardX - 16, 0, self.leaderboardWidth + 16, (self.scoreboardSize + 1) * self.leaderboardHeigth + 16, '800080').setDepth(10)
  .setScrollFactor(0, 0).setAlpha(0.5).setOrigin(0,0);
  for(let i = 0 ; i < size ; i++){
    self.scoreboard.push(       
      self.add.text(
        self.leaderboardX,
        self.leaderboardY + i * self.leaderboardGapY,
        ``, 
        { fontSize: `${self.fontSize}px`, fill: `${self.enemyFontColor}` }
        )
        .setScrollFactor(0, 0).setAlpha(0.75).setDepth(11).setFontFamily('Arial').setFontStyle('Bold')
    );
  }
};

function updateScoreboard(self, oldScores, newScores, size){
  let clientInTop = false;
  for(let i = 0 ; i < size ; i++){
    if(newScores[i] == undefined){
      oldScores[i].setText('');
      continue
    }
    let newScore = newScores[i].score + '  ' + newScores[i].nick;
    if(oldScores[i] == undefined)
    oldScores[i].setText(`${newScore}`);
    else if(oldScores[i].text != newScore)
    oldScores[i].setText(`${newScore}`);
    if(newScores[i].id == self.socket.id){
      oldScores[i].setFill(self.clientFontColor);
      clientInTop = true;
    }
    else
    oldScores[i].setFill(self.enemyFontColor);
  }

  if(clientInTop)
    self.clientScoreText.setVisible(false)
  else
    self.clientScoreText.setVisible(true)
};

function addClientScore(self){
  let x = self.leaderboardX;
  let y = self.leaderboardY + self.scoreboardSize * self.leaderboardGapY

  self.clientScoreText = (self.add.text(
    x, y,
    `${self.clientScoreText}`,
    { fontSize: `${self.fontSize}px`, fill: `${self.clientFontColor}` }
  ).setScrollFactor(0, 0).setDepth(11).setFontFamily('Arial').setFontStyle('Bold')
  );
}

function updateClientScore(self, nick, score){
  self.clientScore = score;
  self.clientScoreText.setText(`${score}  ${nick}`);
}

//Adds a controllable player to the game
function addPlayer(self, playerInfo) {
  self.ship = self.physics.add
    .sprite(playerInfo.x, playerInfo.y, playerInfo.skin)
    .setOrigin(0.5, 0.5)
    .setDisplaySize(50, 40)
    .setRotation(playerInfo.rotation)
    .setCollideWorldBounds(true);
  scene.cameras.main.startFollow(self.ship);
  /*if (playerInfo.team === "blue") {
    self.ship.setTint(0x0000ff);
  } else {
    self.ship.setTint(0xff0000);
  }*/
  self.ship.setDrag(100);
  self.ship.setAngularDrag(100);
  self.ship.body.setMaxSpeed(600).setBounce(0.2, 0.2);
  playerPlayerOverlap(self);

  //self.playerNameText = self.add.text(self.ship.x, self.ship.y, "PlayerTest", {fontSize: '20px', color: '#fff'});
}

function fireBullet(self) {
  self.socket.emit(
    "bulletFiring",
    self.ship.body.velocity.x,
    self.ship.body.velocity.y
  );
}

//Adds opponent players to the game
function addOtherPlayers(self, playerInfo) {
  const otherPlayer = self.add
    .sprite(playerInfo.x, playerInfo.y, playerInfo.skin)
    .setOrigin(0.5, 0.5)
    .setDisplaySize(50, 40)
    .setRotation(playerInfo.rotation);

  /*if (playerInfo.team === "blue") {
    otherPlayer.setTint(0x0000ff);
  } else {
    otherPlayer.setTint(0xff0000);
  }*/
  otherPlayer.playerId = playerInfo.playerId;
  self.otherPlayers.add(otherPlayer);
}

function addBullet(self, bulletData) {
  const bullet = self.physics.add
    .sprite(bulletData.x, bulletData.y, "bullet")
    .setOrigin(0.5, 0.5)
    .setDisplaySize(15, 15)
    .setRotation(bulletData.rotation);
  bulletAsteroidOverlap(self);
  bulletBulletOverlap(self)
  self.bullets.add(bullet);
  //gives the bullet sprite velocity
  bullet.body.setCircle(399);
  bullet.body.setVelocityX(bulletData.velocityX);
  bullet.body.setVelocityY(bulletData.velocityY);
  bullet.timeFired = bulletData.timeFired;
  bullet.ownerId = bulletData.ownerId;
  bullet.bulletId = bulletData.bulletId;
}
// Bullets destroying ships. Ships own bullets cant destroy them for 0.5 sec since the moment of firing
function bulletPlayerOverlap(self) {
  self.physics.add.overlap(
    self.otherPlayers,
    self.bullets,
    function (player, bullet) {
      if (bullet.ownerId == self.socket.id) {
        self.socket.emit("playerShot", player.playerId, bullet.bulletId);
      }
    }
  );
  self.physics.add.overlap(self.ship, self.bullets, function (player, bullet) {
    if (bullet.ownerId == self.socket.id) {
      self.socket.emit('playerShot', self.socket.id, bullet.bulletId);
    }    
  });
}

function playerPlayerOverlap(self) {
  self.physics.add.overlap(self.ship, self.otherPlayers, function(me, enemy) {
    self.socket.emit('playerCollision', self.socket.id, enemy.playerId);
  });
}

function bulletBulletOverlap(self) {
  self.physics.add.overlap(self.bullets, self.bullets, function(bullet1, bullet2) {
    if(bullet1.ownerId == self.socket.id){
      self.socket.emit('bulletDied', bullet1.bulletId, bullet2.bulletId);
    }
  });
}

function addAsteroid(self, asteroidData) {
  const asteroid = self.physics.add
    .sprite(asteroidData.x, asteroidData.y, "asteroid")
    .setDisplaySize(asteroidData.asteroidSize, asteroidData.asteroidSize)
    .setOrigin(1, 1);
  self.asteroids.add(asteroid);
  asteroid.body.setCircle(256);
  asteroid.body.setVelocity(asteroidData.velocityX, asteroidData.velocityY);
  asteroid.asteroidId = asteroidData.asteroidId;
}

// Asteroids destroying ships.
function asteroidPlayerOverlap(self) {
  self.physics.add.overlap(self.ship, self.asteroids, function () {
    self.socket.emit("playerHitByAsteroid", self.socket.id);
  });
}

// Bullets destroying asteroids.
function bulletAsteroidOverlap (self) {
  self.physics.add.overlap(self.asteroids, self.bullets, function (asteroid, bullet) {
    if(bullet.ownerId == self.socket.id)
    {
      self.socket.emit('asteroidShot', asteroid.asteroidId, bullet.bulletId);
    }
    bullet.destroy();
    asteroid.destroy();
  });
}

//Continuously updates player's location and movement parameters
function update() {

  if (this.gameOver == true) {
    window.location.reload(true);
    this.gameOver = false;
  }

  if (this.returnToMenu == true) {
    window.location.href = "/";
    this.returnToMenu = false;
  }

  let angularVelocity = 150;
  if (!this.ship) return;
  if (!this.ship.body) return;

  //rotate ship
  if (this.cursors.left.isDown || keyA.isDown) {
    this.ship.setAngularVelocity(-angularVelocity);
  } else if (this.cursors.right.isDown || keyD.isDown) {
    this.ship.setAngularVelocity(angularVelocity);
  } else {
    this.ship.setAngularVelocity(0);
  }

  //move ship forward
  if (this.cursors.up.isDown || keyW.isDown) {
    this.physics.velocityFromRotation(
      this.ship.rotation + Math.PI / 2,
      600,
      this.ship.body.acceleration
    );
  } else {
    this.ship.setAcceleration(0);
  }

  if (keySpace.isDown || keyX.isDown) {
    fireBullet(this);
  }

  //this.playerNameText.setPosition(this.ship.x, this.ship.y);

  // emit player movement
  const { x, y, rotation } = this.ship;

  if (
    this.ship.oldPosition &&
    (x !== this.ship.oldPosition.x ||
      y !== this.ship.oldPosition.y ||
      rotation !== this.ship.oldPosition.rotation)
  ) {
    this.socket.emit("playerMovement", {
      x: x,
      y: y,
      rotation: rotation,
    });
  }
  // save old position data
  this.ship.oldPosition = {
    x,
    y,
    rotation,
  };
}

function render() {}
