//  Starfield background
function createStarfield (mapWidth, mapHeight)
{
  let pixels = mapWidth * mapHeight
  const pixelSmallStarRatio = 40000
  const pixelBigStarRatio = 400000
  const NumOfSmallStars = Math.trunc(pixels/pixelSmallStarRatio)
  const NumOfBigStars = Math.trunc(pixels/pixelBigStarRatio)
  
  

  var group = scene.add.group({ key: 'backgroundStarSmall', frameQuantity: NumOfSmallStars });

  group.createMultiple({ key: 'backgroundStarBig', frameQuantity: NumOfBigStars });

  var rect = new Phaser.Geom.Rectangle(0, 0, mapWidth, mapHeight);

  Phaser.Actions.RandomRectangle(group.getChildren(), rect);

  group.children.iterate(function (child) {

    var sf = Math.max(0.3, Math.random());

    if (child.texture.key === 'bigStar')
    {
      sf = 0.2;
    }
    //  The scrollFactor values give them their 'parallax' effect
    child.setScrollFactor(sf);

  }, this);
}

export default createStarfield;