const { database } = require("../utils/dbConfig");
const { internalError, notEnoughMoney } = require("../utils/errors");

getShopItems = async (userid) => {
  try {
    const { rows } = await database.query(`
        SELECT * 
        FROM "SpaceRoyale".Shopitems;`);
    let items = rows;
    let ownedItems = await getOwnedItems(userid);

    for (let i = 0; i < items.length; i++) {
      for (let j = 0; j < ownedItems.length; j++) {
        if (items[i].itemid == ownedItems[j].itemid) {
          delete items[i];
          break;
        }
      }
    }
    return items;
  } catch (error) {
    console.log(error);
    return [];
  }
};

getOwnedItems = async (userid) => {
  try {
    const { rows } = await database.query(`
        SELECT shop.itemid, imgname
        FROM "SpaceRoyale".InventoryItems inventory, "SpaceRoyale".ShopItems shop
        WHERE inventory.userid = '${userid}' 
        AND shop.itemid = inventory.itemid;`);
    return rows;
  } catch (error) {
    console.log(error);
    return [];
  }
};

getItemById = async (itemid) => {
  try {
    const { rows } = await database.query(`
        SELECT * 
        FROM "SpaceRoyale".Shopitems
        WHERE itemid = '${itemid}'
        LIMIT 1;`);
    //console.log(rows[0]);
    return rows[0];
  } catch (error) {
    console.log(error);
    return [];
  }
};

const buyItem = async function (req, res, next) {
  const userid = req.user.userid;
  const item = await getItemById(req.params.id);

  try {
    const { rows } = await database.query(`
        UPDATE "SpaceRoyale".Users
        SET currency = currency - ${item.price}
        WHERE userid = ${userid} RETURNING *`);
  } catch (error) {
    console.log(error);
    return res
      .status(internalError.status)
      .render("index", { error: internalError.message });
    return [];
  }

  try {
    const { rows } = await database.query(`
        INSERT INTO "SpaceRoyale".InventoryItems (userid, itemid)
        VALUES ('${userid}', '${item.itemid}')
        RETURNING *;`);
  } catch (error) {
    console.log(error);
    return res
      .status(internalError.status)
      .render("index", { error: internalError.message });
    return [];
  }

  return next();
};

const checkBalance = async function (req, res, next) {
  const userid = req.user.userid;
  const itemid = req.params.id;

  try {
    const { rows } = await database.query(`
        SELECT currency
        FROM "SpaceRoyale".Users
        WHERE userid = '${userid}';`);

    let currency = rows[0].currency; //console.log("currency: " + currency);
    let item = await getItemById(itemid); //console.log("Item price: " + item.price);

    if (currency >= item.price) {
      return next();
    }

    const user = req.user;

    return res.status(notEnoughMoney.status).render("shop", {
      user: user,
      skins: await getShopItems(user.userid),
      errorShop: notEnoughMoney.message,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(internalError.status)
      .render("index", { error: internalError.message });
  }
};

selectSkin = async (userId, itemId) => {
  try {
    const { rows } = await database.query(`
        UPDATE "SpaceRoyale".Users
        SET skinid = ${itemId}
        WHERE userid = ${userId} RETURNING *`);
    return rows;
  } catch (error) {
    console.log(error);
    return [];
  }
};

module.exports = {
  getShopItems,
  getItemById,
  getOwnedItems,
  checkBalance,
  buyItem,
  selectSkin,
};
