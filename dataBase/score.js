const { database } = require("../utils/dbConfig");

function increasePlayerStatInDb(statName, statIncrease, userid){
  if (userid == null)
    return;
  
  const increaseStat = `UPDATE "SpaceRoyale".Player_stats 
                        SET ${statName} = ${statName} + ${statIncrease} 
                        WHERE userid = ${userid} `

  database.query(increaseStat, (err, res) => {
    if (err) {
      console.log(err.stack);
    }
  });
}

function setPlayerStatInDb(statName, stat, userid) {
  if (userid == null)
    return;

  const setStat = `UPDATE "SpaceRoyale".Player_stats 
                   SET ${statName} = ${stat}
                   WHERE userid = ${userid}
                   AND ${statName} < ${stat}`

  database.query(setStat, (err, res) => {
    if (err) {
      console.log(err.stack);
    }
  });
}

function addCurencyforPoints(points, userid){
  if (userid == null)
    return;

  let currencyAdded = Math.trunc(points / 20)
  const addCurency = `UPDATE "SpaceRoyale".Users
                      SET currency = currency + ${currencyAdded}
                      WHERE userid = ${userid}`

  database.query(addCurency, (err, res) => {
    if (err) {
      console.log(err.stack);
    }
  });
}

module.exports = {
  addCurencyforPoints,
  increasePlayerStatInDb,
  setPlayerStatInDb
};
