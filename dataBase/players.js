const { database } = require("../utils/dbConfig");

getUsers = async () => {
  try {
    const { rows } = await database.query(`
        SELECT nickname, total_score
        FROM "SpaceRoyale".Player_stats AS stats
        JOIN "SpaceRoyale".Users AS users
        ON users.userid = stats.userid
        ORDER BY total_score desc;`);

    return rows;
  } catch (error) {
    console.log(error);
    return [];
  }
};

getPersonalScores = async (userid) => {
  try {
    const { rows } = await database.query(`
        SELECT *
        FROM "SpaceRoyale".Player_stats 
        WHERE userid = '${userid}';`);
    return rows[0];
  } catch (error) {
    console.log(error);
    return [];
  }
};

module.exports = {
  getUsers,
  getPersonalScores,
};
