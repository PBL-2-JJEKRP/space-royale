const {
  internalError,
  nicknameInUse,
  emailInUse,
  incorrectCredentials,
} = require("../utils/errors");
const { database } = require("../utils/dbConfig");
const { hashPassword, compare } = require("../utils/hashing");

deleteUsers = async () => {
  try {
    const { rows } = await database.query(`
        DELETE FROM "SpaceRoyale".Users;`);
    //console.log("Owned Items:");
    console.log("deleted");
    return rows;
  } catch (error) {
    console.log(error);
    return [];
  }
};

countUsers = async () => {
  try {
    const { rows } = await database.query(
      `SELECT COUNT (*) FROM "SpaceRoyale".Users;`
    );
    return rows[0].count;
  } catch (error) {
    console.log(error);
    return [];
  }
};

function checkNickname(req, res, next) {
  const nickname = req.body.nickname;
  const nicknameUnique = `SELECT * FROM "SpaceRoyale".Users WHERE nickname = '${nickname}' LIMIT 1`;

  database.query(nicknameUnique, (err1, res1) => {
    if (err1) {
      console.log(err1.stack);
      return res
        .status(internalError.status)
        .render("index", { error: internalError });
    }
    if (res1.rows[0]) {
      return res
        .status(nicknameInUse.status)
        .render("signup", { errorNickname: nicknameInUse.message });
      //resError(res, 500, "This nickname is already in use");
    }
    return next();
  });
}

function checkEmail(req, res, next) {
  const email = req.body.email;
  const emailUnique = `SELECT * FROM "SpaceRoyale".Users WHERE email = '${email}' LIMIT 1`;

  database.query(emailUnique, (err1, res1) => {
    if (err1) {
      console.log(err1.stack);
      return res
        .status(internalError.status)
        .render("signup", internalError.message);
    }
    if (res1.rows[0]) {
      return res
        .status(emailInUse.status)
        .render("signup", { errorEmail: emailInUse.message });
    }
    return next();
  });
}

function addUser(req, res, next) {
  const { nickname, email } = req.body;
  const hashedPassword = hashPassword(req.body.password);
  let insertData = `WITH data(nickname, email, hashedPassword) 
  AS (VALUES ('${nickname}', '${email}', '${hashedPassword}') ),
  ins1 AS (
   INSERT INTO "SpaceRoyale".Users (nickname, email, hash)
   SELECT nickname, email, hashedPassword
   FROM   data
   RETURNING userid AS id
   ), ins2 AS (
   INSERT INTO "SpaceRoyale".Player_stats (userid)
   SELECT id FROM ins1
   )
   INSERT INTO "SpaceRoyale".inventoryitems (userid)
   (SELECT id from ins1);`;

  database.query(insertData, (err1, res1) => {
    if (err1) {
      console.log(err1.stack);
      return res
        .status(internalError.status)
        .render("index", { error: internalError.message });
    }
    return res.render("login", {
      success: "You have successfully created an account",
    });
    return next();
  });
}

const authenticateUser = async (email, password, done) => {
  try {
    const { rows } = await database.query(`
        SELECT * FROM "SpaceRoyale".Users WHERE email = '${email}';`);
    if (rows.length > 0) {
      const user = rows[0];
      if (compare(password, user.hash)) {
        return done(null, user);
      }
      return done(null, false, { message: incorrectCredentials.message });
    }

    // No user
    return done(null, false, { message: incorrectCredentials.message });
  } catch (error) {
    console.log(error);
    return done(null, false, {
      message: internalError.message,
    });
  }
};

const getUserById = async (id) => {
  try {
    const { rows } = await database.query(`
        SELECT userid, nickname, email, currency, skinid  FROM "SpaceRoyale".Users WHERE userid = '${id}';`);
    if (rows.length > 0) {
      const user = rows[0];
      return user;
    }
    //No user found with this id
    console.log("No user found with this id");
    return null;
  } catch (error) {
    console.log(error);
    return null;
  }
};

module.exports = {
  checkNickname,
  checkEmail,
  addUser,
  authenticateUser,
  getUserById,
};
