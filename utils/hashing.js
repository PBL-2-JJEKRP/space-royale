"use strict";
let crypto = require("crypto");

const num = 75; //position of a character that will impact noise position in hash
const noiseLength = 4;
const saltPosition = 93; // Where salt will be inserted in to hash
const saltLength = 12;
const saltRounds = 12;

const hashPassword = (password) => {
  let salt = generateSalt(saltRounds);
  let hashedPassword = doHash(password, salt);
  hashedPassword = insertSalt(hashedPassword, salt);
  hashedPassword = insertNoise(hashedPassword);
  return hashedPassword;
};

//Creates a random saltLength digit hexadecimal number
let generateSalt = (rounds) => {
  if (rounds >= 15) {
    throw new Error(`${rounds} is greater than 15,Must be less that 15`);
  }
  if (typeof rounds !== "number") {
    throw new Error("rounds param must be a number");
  }
  if (rounds == null) {
    rounds = saltLength;
  }
  return crypto
    .randomBytes(Math.ceil(rounds / 2))
    .toString("hex")
    .slice(0, rounds);
};

let doHash = (password, salt) => {
  if (password == null || salt == null) {
    throw new Error("Must Provide Password and salt values");
  }
  if (typeof password !== "string" || typeof salt !== "string") {
    throw new Error(
      "password must be a string and salt must either be a salt string or a number of rounds"
    );
  }

  let hash = crypto.createHmac("sha512", salt);
  hash.update(password);
  let value = hash.digest("hex");
  return value;
};

//Checks if hash of the salted password is equal to a given hash
let compare = (password, hash) => {
  if (password == null || hash == null) {
    throw new Error("password and hash is required to compare");
  }
  hash = removeNoise(hash);
  let salt = getSalt(hash);
  let passwordDataString = doHash(password, salt);
  passwordDataString = insertSalt(passwordDataString, salt).toString();

  if (passwordDataString === hash) {
    return true;
  }
  return false;
};
// Inserts a salt inside into a hash
function insertSalt(hash, salt) {
  let newHash = hash;
  let reversedSalt = salt.split("").reverse().join("");
  newHash =
    newHash.substring(0, saltPosition) +
    reversedSalt +
    newHash.substring(saltPosition);
  return newHash;
}
// Inserts random characters that will be ignored when comparing passwords
function insertNoise(hash) {
  let noise = generateSalt(noiseLength);
  let noiseInsertPos = parseInt(hash[num], 16) + 1;
  return (
    hash.substring(0, noiseInsertPos) + noise + hash.substring(noiseInsertPos)
  );
}

function removeNoise(hash) {
  let cleanHash;
  let noisePos = parseInt(hash[num + noiseLength], 16) + 1;

  cleanHash = hash.slice(0, noisePos) + hash.slice(noisePos + noiseLength);
  return cleanHash;
}

function getSalt(hash) {
  let salt;
  salt = hash.slice(saltPosition, saltPosition + saltLength);
  salt = salt.split("").reverse().join("");
  return salt;
}

module.exports = {
  hashPassword,
  compare,
};
