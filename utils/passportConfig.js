const LocalStrategy = require("passport-local").Strategy;
const { authenticateUser, getUserById } = require("../dataBase/user");
const { internalError } = require("./errors");

function initialize(passport) {
  passport.use(
    new LocalStrategy(
      { usernameField: "email", passwordField: "password" },
      authenticateUser
    )
  );
  passport.serializeUser((user, done) => {
    done(null, user.userid);
  });
  passport.deserializeUser(async (id, done) => {
    const result = await getUserById(id);
    if (result == null) {
      // if some db errors2
      return done(null, false, { message: internalError.message });
    }
    return done(null, result); //result == user
  });
}

module.exports = initialize;
