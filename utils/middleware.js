const { notAuthSrc } = require("./errors");
const { players } = require("../dataBase/players");

function checkAuth(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.status(notAuthSrc.status).render("index", { error: notAuthSrc });
}

function checkNotAuth(req, res, next) {
  if (req.isAuthenticated()) {
    res
      .status(notAuthSrc.status)
      .render("user", { user: req.user, players: players, error: notAuthSrc });
    return;
  }
  next();
}

module.exports = {
  checkAuth,
  checkNotAuth,
};
