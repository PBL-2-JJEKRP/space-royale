let maxAsteroidSize = 256;

let createSingleAsteroid = (asteroids, mapWidth, mapHeight) => {
    let rotation = Math.random();
    let x, y;
    switch (true) { // Sets asteroids starting position
        case (rotation < 0.125): // Top
          x = Math.floor(Math.random() * (mapWidth + maxAsteroidSize) ) - maxAsteroidSize/2;
          y = -(maxAsteroidSize) + 1;
          break;
        case (rotation < 0.375): // Right
          x = mapWidth + maxAsteroidSize - 1;
          y = Math.floor(Math.random() * (mapHeight + maxAsteroidSize)) - maxAsteroidSize/2;
          break;
        case (rotation < 0.625): // Bottom
          x = Math.floor(Math.random() * (mapWidth + maxAsteroidSize) ) - maxAsteroidSize/2;
          y = mapHeight + maxAsteroidSize - 1;
          break; 
        case (rotation < 0.875):  // Left
          x = -(maxAsteroidSize) + 1;
          y = Math.floor(Math.random() * (mapHeight + maxAsteroidSize)) - maxAsteroidSize/2;
          break;
        default: // Top
          x = Math.floor(Math.random() * (mapWidth + maxAsteroidSize) ) - maxAsteroidSize/2;
          y = -(maxAsteroidSize) + 1;
          break;
          
            
    }
    let d = new Date();
    let currTime = d.getTime();
    let idIdentifier = 'ast';
    let uniquestring = idIdentifier.concat(currTime).concat(x).concat(y);

    // save the data of the asteroid in the server
    let maxSpeed = 280;
    let speed = Math.random(0.01, 1) * maxSpeed;
    rotation *= 2 * Math.PI
    let asteroidSize = Math.floor(Math.random() * 206 + 50);
    asteroids[uniquestring] = {
        x: x,
        y: y,
        velocityX: Math.floor(-Math.sin(rotation) * speed),
        velocityY: Math.floor(Math.cos(rotation) * speed),
        timeLastUpdated: currTime,
        asteroidId: uniquestring,
        asteroidSize: asteroidSize
    };
    return uniquestring;
};

// Create multiple asteroids.js with different locations, ids
let createAsteroids = (count, asteroids, mapWidth, mapHeight) => {
    for (let i = 0; i < count; i++){
        createSingleAsteroid(asteroids, mapWidth, mapHeight);
    }
    return false;
};

function asteroidOutOfBounds(asteroidX, asteroidY, mapWidth, mapHeight){
  if(asteroidX < 0 - maxAsteroidSize || asteroidY < 0 - maxAsteroidSize
     || asteroidX > mapWidth + maxAsteroidSize || asteroidY > mapHeight + maxAsteroidSize)
    return true;
  return false;
}

module.exports = {
  asteroidOutOfBounds,
  createSingleAsteroid,
  createAsteroids
}