let sortAfterUpdate = (array, id, oldScore, scoreDiff) => {
  let insertIndex;

  let aprxIndex = binarySearch(array, oldScore, 0, array.length - 1, false);
  if(aprxIndex == -1){
    console.log("ERROR: player with a given score in leaderboard is not found")
    return;
  }
  let index = idSearch(array, aprxIndex, id, oldScore);
  array[index].score += scoreDiff;
  // Sorting the scoreboard
  let temp = array[index];
  array.splice(index, 1) //removing the previous score

  if(scoreDiff > 0){
    let playerIsLast = true;
    insertIndex = binarySearch(array, oldScore + scoreDiff, 0, array.length - 1, true)
    if(array.length == 0){
      array.splice(insertIndex, 0, temp);
      return
    }
    
    while(insertIndex < array.length){
      if(array[insertIndex].score < oldScore + scoreDiff){
        playerIsLast = false;
        break;
      }
      insertIndex++;
    }
    if(playerIsLast) insertIndex++;
    array.splice(insertIndex, 0, temp); //inserting updated score
  }

  else{
    let playerIsLast = true;
    insertIndex = binarySearch(array, oldScore + scoreDiff, 0, array.length - 1, true)
    //console.log(insertIndex)
    if(array.length == 0){
      array.splice(insertIndex, 0, temp);
      return
    }
    
    while(insertIndex < array.length){
      if(array[insertIndex].score < oldScore + scoreDiff){
        playerIsLast = false;
        break;
      }
      insertIndex++;
    }
    if(playerIsLast) insertIndex++;
    array.splice(insertIndex, 0, temp); //inserting updated score
  }


}

//If approximate = true, returns index into which a key should be inserted in sortedArray
function binarySearch(sortedArray, key, start, end, approximate){ //searches for score
  let middle = Math.floor((start + end) / 2);
  while (start <= end) {
    
    middle = Math.floor((start + end) / 2); 
    if (sortedArray[middle].score == key) {
      // found the key
      return middle;
    } else if (sortedArray[middle].score > key) {
      // continue searching to the right
      start = middle + 1;
    } else {
      // continue searching to the left
      end = middle - 1;
    }
  }
// key wasn't found
  if(approximate)
    return middle;
  return -1;  
}
// returns index of a user with a given id
function idSearch(sortedArray, aprxIndex, id, score){
  let i = aprxIndex
  while(i < sortedArray.length){ // Searches to the right
    if(sortedArray[i].score != score)
      break
    if(sortedArray[i].id == id)
      return i
    i++    
  }

  i = aprxIndex - 1
  while(i >= 0){ // Searches to the left
    if(sortedArray[i].score != score)
      break
    if(sortedArray[i].id == id)
      return i
    i++
  }
  return -1;
}

module.exports = {
  sortAfterUpdate,
}