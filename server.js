if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io").listen(server);
const {addCurencyforPoints, increasePlayerStatInDb, setPlayerStatInDb} = require("./dataBase/score.js")
const {router} = require("./router/router");

//const middleware = require("./utils/middleware");
const {
  calculateCurrentBulletPositions,
  ableToFire,
  createBullet,
} = require("./game/bullets.js");
const {
  asteroidOutOfBounds,
  createSingleAsteroid,
  createAsteroids,
} = require("./game/asteroids.js");
const { sortAfterUpdate } = require("./game/scoreboard.js");
const passport = require("passport");
const flash = require("express-flash");
const session = require("express-session");
const expressLayouts = require("express-ejs-layouts");
const { internalError } = require("./utils/errors");

//passport and session
const initializePassport = require("./utils/passportConfig");
initializePassport(passport);

//const mapWidth = 2400;
//const mapHeight = 1350;
const mapWidth = 6400;
const mapHeight = 3200;

let bulletLimit = 5; // Limit of active bullets a player can have at a time
let bulletReload = 500; // Time after firing during which the player cannot fire (measured in ms)
let bulletVelocity = 400; // Speed at which the bullets travel (measured in pixels/s)

let bulletUpdateInterval = 500; // How often server updates bullet positions
let asteroidUpdateInterval = 100; // How often server updates asteroid positions

// How much can bullets/asteroids.js desynchronize between client and server before getting redrawn.
let maximumBulletDeviation = 10; // Measured in pixels.
let maximumAsteroidDeviation = 10; // Measured in pixels.

let currPlayerCount = 0;
let players = {};
let bullets = {};
let scoreTable = [];
let scoreTableSize = 5;

let starPoints = 5;
let killPoints = 10;
let stealPercent = 0.2;

let asteroids = {};
let asteroidCount = 30;

// How long will the player be invulnerable to asteroid collisions after spawning.
let invulnerability = 1000;

var star = {
  id: 0,
  x: Math.floor(Math.random() * (mapWidth - 106)) + 53,
  y: Math.floor(Math.random() * (mapHeight - 80)) + 40,
};

app.use(express.urlencoded({ extended: false })); // Express body parser
app.use(flash()); //
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    //store: sessionStore,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24, // Equals for 1 day (1day, 24h, 60min, 60s)
    },
  })
); //

app.use(passport.initialize());
app.use(passport.session());
app.use(expressLayouts);

app.set("layout", __dirname + "/public/views/layout");
app.set("views", __dirname + "/public/views");
app.set("view engine", "ejs");

app.use(express.static(__dirname + "/public")); //to serve static js/css files
//app.use(middleware.loggingMiddleware);
app.use("/", router);

// Updates the position of all bullets every interval miliseconds
function calculateBulletPositionsOnInterval(
  bullets,
  players,
  mapWidth,
  mapHeight,
  bulletUpdateInterval
) {
  calculateCurrentBulletPositions(bullets, players, mapWidth, mapHeight);
  io.emit("bulletUpdate", bullets, maximumBulletDeviation);
  setTimeout(
    calculateBulletPositionsOnInterval,
    bulletUpdateInterval,
    bullets,
    players,
    mapWidth,
    mapHeight,
    bulletUpdateInterval
  );
}

let calculateCurrentAsteroidPositions = (asteroids, mapWidth, mapHeight) => {
  let d = new Date();
  let currTime = d.getTime();
  // amount of time that has passed since last bullet update measured in miliseconds
  let timeDiff;
  Object.keys(asteroids).forEach(function (id) {
    timeDiff = currTime - asteroids[id].timeLastUpdated;
    asteroids[id].x += (asteroids[id].velocityX * timeDiff) / 1000; //division is needed because timeDiff is measured in ms
    asteroids[id].y += (asteroids[id].velocityY * timeDiff) / 1000; //but velocity is measured in px/s
    asteroids[id].timeLastUpdated = currTime;

    if (
      asteroidOutOfBounds(asteroids[id].x, asteroids[id].y, mapWidth, mapHeight)
    ) {
      asteroidDied(id);
    }
  });
};

calculateBulletPositionsOnInterval(
  bullets,
  players,
  mapWidth,
  mapHeight,
  bulletUpdateInterval
);
calculateAsteroidPositionsOnInterval(
  asteroids,
  mapWidth,
  mapHeight,
  asteroidUpdateInterval
);

createAsteroids(asteroidCount, asteroids, mapWidth, mapHeight);

// when a player connects to the server
io.on("connection", function (socket) {
  currPlayerCount++;
  let d = new Date();
  let currTime = d.getTime();
  console.log("a user connected: ", socket.id);
  // create a new player and add it to our players object
  players[socket.id] = {
    rotation: Math.random() * Math.PI * 2,
    x: Math.floor(Math.random() * (mapWidth - 100)) + 50,
    y: Math.floor(Math.random() * (mapHeight - 100)) + 50,
    playerId: socket.id,
    userid: null,
    nickname: "Pilot_" + socket.id.substring(0, 3),
    skin: "skin00",
    score: 0,
    kills: 0,
    deathCause: "Still alive",
    // tracks the amount of currently active bullets the player has fired
    bulletCount: 0,
    lastBulletFiredDate: currTime,
  };

  socket.on("sendUser", function (user) {
    if (players[socket.id] == undefined) return;
    if (user.id) players[socket.id].userid = user.id
    if (user.nickname) players[socket.id].nickname = user.nickname;
    if (user.ship) {
      let skin = user.ship.split("/");
      skin = skin[skin.length - 1].split(".");
      skin = skin[0];
      players[socket.id].skin = skin;
    }
    scoreTable.push({
      id: socket.id,
      nick: players[socket.id].nickname,
      score: 0, //Causes problems if some players have a negative score-
    });

    socket.emit("sendServerConfiguration", mapWidth, mapHeight, scoreTableSize);
    // send the players object to the new player
    socket.emit("currentPlayers", players, invulnerability);
    calculateCurrentBulletPositions(bullets, players, mapWidth, mapHeight);
    calculateCurrentAsteroidPositions(asteroids, mapWidth, mapHeight);
    // send the bullets object to the new player
    socket.emit("currentBullets", bullets);
    // send the asteroids.js object to the new player
    socket.emit("currentAsteroids", asteroids);
    // send the star object to the new player
    socket.emit("starLocation", star);
    // update all other players of the new player
    socket.broadcast.emit("newPlayer", players[socket.id], socket.id);

    updateScore(scoreTable, players, socket.id, 0);
  });

  // when a player disconnects, remove them from our players object
  socket.on("disconnect", function () {
    console.log("user disconnected: ", socket.id);
    if (players[socket.id] == undefined) 
      return

    if (players[socket.id].deathCause != "Still alive") 
      return

    sendOnDeathDataToDatabase(players[socket.id]);
    playerDied(socket.id, true);
  });

  // when a player moves, update the player data
  socket.on("playerMovement", function (movementData) {
    if (players[socket.id] != undefined) {
      players[socket.id].x = movementData.x;
      players[socket.id].y = movementData.y;
      players[socket.id].rotation = movementData.rotation;
      // emit a message to all players about the player that moved
      // Might want to make this a volatile emit https://socket.io/docs/v3/emitting-events/
      socket.broadcast.emit("playerMoved", players[socket.id]);
    }
  });

  // when a player tries to fire, perform checks, save and emit bullet data if a shot is legit
  socket.on("bulletFiring", function (shipVelocityX, shipVelocityY) {
    if (players[socket.id] == undefined) return;
    if (!ableToFire(players[socket.id], bulletLimit, bulletReload)) return;

    let bulletId = createBullet(
      bullets,
      bulletVelocity,
      players[socket.id],
      shipVelocityX,
      shipVelocityY
    );
    io.emit("bulletFired", bullets[bulletId]);
  });

  socket.on("playerShot", function (victimId, bulletId) {
    let killerId = socket.id;
    if (
      players[victimId] == undefined ||
      players[killerId] == undefined ||
      bullets[bulletId] == undefined
    )
      return;
    if (killerId == victimId) {
      let d = new Date();
      let currTime = d.getTime();
      if (currTime - bullets[bulletId].timeFired < 500) return;
      players[victimId].deathCause = "death_by_own_shot";
    } else if (killerId != victimId) {
      let scoreAdded =
        killPoints + Math.floor(players[victimId].score * stealPercent);
      updateScore(scoreTable, players, killerId, scoreAdded);
      players[killerId].kills++;
      increasePlayerStatInDb("total_score", scoreAdded, players[socket.id].userid);
      increasePlayerStatInDb("total_kills", 1, players[socket.id].userid);
      players[victimId].deathCause = "death_by_enemy_shot";
    }
    bulletDied(bulletId);
    playerDied(victimId, false);
  });

  socket.on("bulletDied", function (id1, id2) {
    if (bullets[id1] == undefined || bullets[id2] == undefined) return;

    bulletDied(id1);
    bulletDied(id2);
  });

  socket.on("playerHitByAsteroid", function () {
    if (players[socket.id] == undefined) return;
    players[socket.id].deathCause = "death_by_asteroid_crash";
    playerDied(socket.id, false);
  });

  socket.on("playerCollision", function (id1, id2) {
    if (players[id1] == undefined || players[id2] == undefined) return;
    players[id1].deathCause = "death_by_collision_with_other_ship";
    players[id2].deathCause = "death_by_collision_with_other_ship";
    playerDied(id1, false);
    playerDied(id2, false);
  });

  socket.on("asteroidShot", function (asteroidId, bulletId) {
    if (asteroids[asteroidId] == undefined || bullets[bulletId] == undefined)
      return;

    bulletDied(bulletId);
    asteroidDied(asteroidId);
  });

  socket.on("I died", function (playerData) {
    sendOnDeathDataToDatabase(playerData);
  });

  // when a player picks up a star
  socket.on("starCollected", function (starId) {
    if (starId != star.id) return;

    respawnStar();
    io.emit("starLocation", star);
    updateScore(scoreTable, players, socket.id, starPoints);
    increasePlayerStatInDb("total_score", starPoints, players[socket.id].userid);
  });
});

calculateBulletPositionsOnInterval(
  bullets,
  players,
  mapWidth,
  mapHeight,
  bulletUpdateInterval
);
calculateAsteroidPositionsOnInterval(
  asteroids,
  mapWidth,
  mapHeight,
  asteroidUpdateInterval
);

app.use(async (err, req, res, next) => {
  console.error(err);
  if (req.user != null) {
    res.status(internalError.status).render("user", {
      user: req.user,
      players: await require("./players").getUsers(),
      skins: await require("./shop").getItems("skin"),
      error: internalError,
    });
  } else {
    res.status(internalError.status).render("index", { error: internalError });
  }
});

server.listen(process.env.PORT, function () {
  console.log(`Listening on ${server.address().port}`);
  console.log(`Jump to http://localhost:${server.address().port}`);
});

function respawnStar() {
  star.x = Math.floor(Math.random() * (mapWidth - 100)) + 50;
  star.y = Math.floor(Math.random() * (mapHeight - 100)) + 50;
  star.id++;
}

function updateScore(scores, players, playerId, scoreDiff) {
  sortAfterUpdate(scores, playerId, players[playerId].score, scoreDiff);
  players[playerId].score += scoreDiff;
  io.to(playerId).emit(
    "updateClientScore",
    players[playerId].nickname,
    players[playerId].score
  );
  io.emit("scoreUpdate", scores.slice(0, scores.length));
}

function playerDied(id, disconnected) {
  if (!disconnected)
    io.to(id).emit("initiateOnDeathDataTransferToDb", players[id]);

  delete players[id];
  // emit a message to all players to remove this player
  io.emit("playerDied", id);
  for (let i = 0; i < currPlayerCount; i++) {
    if (scoreTable[i] == undefined) break;
    if (scoreTable[i].id == id) {
      scoreTable.splice(i, 1); // removes the player from scoreboard
      break;
    }
  }
  io.emit("scoreUpdate", scoreTable.slice(0, scoreTable.length));
  currPlayerCount--;
}

function sendOnDeathDataToDatabase(playerData) {
  setPlayerStatInDb("best_score", playerData.score, playerData.userid);
  setPlayerStatInDb("best_kills", playerData.kills, playerData.userid);
  increasePlayerStatInDb("total_deaths", 1, playerData.userid);
  increasePlayerStatInDb(playerData.deathCause, 1, playerData.userid);
  addCurencyforPoints(playerData.score, playerData.userid);
}

function bulletDied(id) {
  if (bullets[id] == undefined) return;

  if (players[bullets[id].ownerId] != undefined) {
    players[bullets[id].ownerId].bulletCount--;
  }
  io.emit("bulletDied", id);
  delete bullets[id];
}

function asteroidDied(id) {
  io.emit("asteroidDied", id);
  delete asteroids[id];
  let asteroidId = createSingleAsteroid(asteroids, mapWidth, mapHeight);
  io.emit("asteroidAdded", asteroids[asteroidId]);
}

// Updates the position of all bullets every interval miliseconds
function calculateBulletPositionsOnInterval(
  bullets,
  players,
  mapWidth,
  mapHeight,
  bulletUpdateInterval
) {
  calculateCurrentBulletPositions(bullets, players, mapWidth, mapHeight);
  io.emit("bulletUpdate", bullets, maximumBulletDeviation);
  setTimeout(
    calculateBulletPositionsOnInterval,
    bulletUpdateInterval,
    bullets,
    players,
    mapWidth,
    mapHeight,
    bulletUpdateInterval
  );
}

function calculateAsteroidPositionsOnInterval(
  asteroids,
  mapWidth,
  mapHeight,
  interval
) {
  calculateCurrentAsteroidPositions(asteroids, mapWidth, mapHeight);
  io.emit("asteroidUpdate", asteroids, maximumAsteroidDeviation);
  setTimeout(
    calculateAsteroidPositionsOnInterval,
    interval,
    asteroids,
    mapWidth,
    mapHeight,
    interval
  );
}
